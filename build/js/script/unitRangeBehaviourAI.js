// Put user code here //
const unitRangeBehaviourAI = 
`@fallback{
	@sequence{
		ifDead
		destroyed
	}
    checkEffect
	@sequence{
	    haveTarget
	    isTargetAlive
	    isTargetInRange
	    attackTarget
	}
	findTarget
	moveForward
}`;
//  End of user code  //
