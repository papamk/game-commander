// Put user code here //
game.util = {};

game.util.spread = function(obj, clone, keys){
    for(var x in clone){
        if(keys.includes(x)){
            obj[x] = clone[x];
        }
    }
}

verify = function(obj, keys){
    keys.forEach(function(key){
        if(typeof obj[key] === "undefined"){
            throw `This object ${obj} must have variable ${key}`;
        }
    })
}

game.util.scale = function(obj, val){
    obj.currentTransform.translate(obj.pos.x, obj.pos.y);
    obj.scale(val, val);
    obj.currentTransform.translate(-obj.pos.x, -obj.pos.y);
    
    let bounds = obj.getBounds();
    obj.width = bounds.width;
    obj.height = bounds.height;
}

impose = function(obj, key, defaultVal){
    if(typeof obj[key] === "undefined"){
        if(typeof defaultVal === "undefined"){
            throw `This variable ${key} cannot be undefined inside ${obj}`;
        }
        obj[key] = defaultVal;
    }
}

game.util.spreadAll = function(obj, clone){
    for(var x in clone){
        obj[x] = clone[x];
    }
}

//  End of user code  //
