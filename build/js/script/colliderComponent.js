// Put user code here //
game.component.collider = me.Entity.extend({
    init: function(parent, settings = {}){
        this.parent = parent;
        impose(settings, "radius", 100);
        impose(settings, "shapes", [me.pool.pull("me.Ellipse", settings.radius, settings.radius, settings.radius * 2, settings.radius * 2)]);
        impose(settings, "onCollide", function(){});
        impose(settings, "colliderType", me.collision.types.ENEMY_OBJECT);
        impose(settings, "colliderMask", me.collision.types.ALL_OBJECT);
        impose(settings, "isSolid", false);
        impose(settings, "offsetX", 0);
        impose(settings, "offsetY", 0);
        
        this._super(me.Entity, 'init', [parent.pos.x - settings.radius, parent.pos.y - settings.radius, {
            width: settings.radius,
            height: settings.radius,
            shapes: settings.shapes
        }]);
        
        this.alpha = 1;
        this.floating = false;
        this.alwaysUpdate = true;
        this.updateWhenPaused = false;
        this.isPersistent = false;
        this.body.gravity.y = 0;
        
        game.util.spreadAll(this, settings);
        this.body.collisionType = this.colliderType;
        this.body.setCollisionMask(this.colliderMask);
        console.log(this.colliderMask);
        console.log(this.colliderType);
    },
    
    update: function(dt){
        var drawNextFrame = this._super(me.Entity, 'update', [dt]);
        
        this.body.update();
        drawNextFrame = drawNextFrame || this.body.vel.x !== 0 || this.body.vel.y !== 0;
        
        this.updatePos();
        return drawNextFrame;
    },
    
    updatePos: function(){
        this.pos.x = this.parent.pos.x - this.radius + this.offsetX;
        this.pos.y = this.parent.pos.y - this.radius + this.offsetY;
    },
    
    onCollision: function(response, other){
        if(typeof this.onCollide === 'function'){
            this.onCollide(response, other);
            console.log("response")
        }
        return this.isSolid;
    },
    
    checkCollision: function(){
        // because collision check need ancestor 
        // and we didn't add child to world
        // we need the reference ancestor from parent
        this.ancestor = this.parent.ancestor;
        return me.collision.check(this);
    }
});

me.pool.register("game.component.collider", game.component.collider, true);
//  End of user code  //
