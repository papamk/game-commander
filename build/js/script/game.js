// Put user code here //
game.component = {};

game.data = {
    playerId:'',
    players:{},
    mainPlayer:{},
    pelurulist:{}
}

function addMainPlayer(data){
    if(!data){
      return;
    }
    var mainPlayer = me.pool.pull("player", data.p.x, data.p.y, {
        id: data.id,
        hp: data.hp
    });
 
    game.data.players[data.id] = mainPlayer;
    me.game.world.addChild(mainPlayer, 100);
}

function addOtherPlayer(data){
      if (!data || game.data.players[data.id]) {
            return;
        }
          var players = me.pool.pull("enemy", data.p.x, data.p.y,{
              id: data.id,
              hp: data.hp
          });
           
          game.data.players[data.id] = players;
          me.game.world.addChild(players, 100);
}

function updatePlayerState(data){
    //console.log(data);
     var player = game.data.players[data.id];
           if(player){
                player.state = data.s;
                player.pos.x = data.p.x;
                player.pos.y = data.p.y;
           }
}

function removePlayers(data){
    var enemy = game.data.players[data.id];
        me.game.world.removeChild(enemy);
        delete game.data.players[data.id];
}

function addPelurulain(data){
        if (!data || game.data.pelurulist[data.id]) {
            return;
        }
        var peluru = me.pool.pull("pelurulain", data.p.x, data.p.y);
        game.data.pelurulist[data.id] = peluru;
        me.game.world.addChild(peluru, 101);
}

function updateposisipeluru(data){
    //console.log(data);
     var peluru = game.data.pelurulist[data.id];
           if(peluru){
                peluru.state = data.s;
                peluru.pos.x = data.p.x;
                peluru.pos.y = data.p.y;
           }
}

function updatehp(data){
    //console.log(data);
    var player = game.data.players[data.id];
    if(player){
                player.hp = data.hp;
           }
}


//  End of user code  //
