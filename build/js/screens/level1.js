(function(){
// Put user code here //
 
//  End of user code  //

game.level.level1 = me.Stage.extend({
	onResetEvent: function() {
        // Put user code here //
        
        //  End of user code  //
		me.levelDirector.loadLevel("level1");
		this.var = {};
        // Put user code here //
        game.var.socket = me.plugins.SocketIO;
        game.var.socket.open("192.168.17.96:2000");
        
        this.sessionId = (Math.random()+1).toString(36).slice(2, 18);
        var name = String(this.sessionId);
        game.data.playerId = name;
        game.var.socket.emit('gameReady', { id: game.data.playerId });
        
        game.var.socket.onEvent('playerId', function(data){
            game.data.playerId = data;
        });
        
        game.var.socket.onEvent('addMainPlayer', function (data) {
            addMainPlayer(data);
        });
        
        game.var.socket.onEvent('addPlayer', function(data){
            //console.log(data);
            addOtherPlayer(data);
        });
        
        game.var.socket.onEvent('addPlayers', function(data){
            for(var id in data) {
                if(id != game.data.playerId) {
                    addOtherPlayer(data[id]);
                }
            }
        });
        
        game.var.socket.onEvent('updatePlayerState', function(data){
            updatePlayerState(data);
        });
        
        game.var.socket.onEvent('removePlayer', function(data){
            if(game.data.playerId == data ){
                window.location ='/';
            }
             removePlayers({id:data});
        });
        
        //PELURU TEMBAK
        
        game.var.socket.onEvent('addpeluru', function(data){
           addPelurulain(data);

        });
        
        game.var.socket.onEvent('addpelurus', function(data){
            for(var id in data) {
                if(id != game.data.playerId) {
                    addPelurulain(data[id]);
                }
            }
        });
        
        game.var.socket.onEvent('updateposisipeluru', function(data){
            //console.log(data);
            updateposisipeluru(data);
        });
        
        game.var.socket.onEvent('hpupdate', function(data){
            updatehp(data);
        });
        
        //  End of user code  //
	},

	onDestroyEvent: function() {
        // Put user code here //
        
        //  End of user code  //
	},

    // Put user code here //
    
    //  End of user code  //
});

// Put user code here //
 
//  End of user code  //
})();