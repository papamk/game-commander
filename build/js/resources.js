game.resources = [
	{
		"name": "level_prison_lv1",
		"type": "image",
		"src": "data/image/level_prison_lv1.png"
	},
	{
		"name": "shadow",
		"type": "image",
		"src": "data/image/shadow.png"
	},
	{
		"name": "level1",
		"type": "tmx",
		"src": "data/level/level1.json"
	},
	{
		"name": "prisoner1",
		"type": "tmx",
		"src": "data/level/prisoner1.json"
	},
	{
		"name": "texture_army_0",
		"type": "image",
		"src": "data/image/texture_army_0.png"
	},
	{
		"name": "texture_army_0",
		"type": "json",
		"src": "data/json/texture_army_0.json"
	},
	{
		"name": "texture_bglv_0",
		"type": "image",
		"src": "data/image/texture_bglv_0.png"
	},
	{
		"name": "texture_bglv_0",
		"type": "json",
		"src": "data/json/texture_bglv_0.json"
	},
	{
		"name": "texture_env_0",
		"type": "image",
		"src": "data/image/texture_env_0.png"
	},
	{
		"name": "texture_env_0",
		"type": "json",
		"src": "data/json/texture_env_0.json"
	},
	{
		"name": "texture_german_sniper_0",
		"type": "image",
		"src": "data/image/texture_german_sniper_0.png"
	},
	{
		"name": "texture_german_sniper_0",
		"type": "json",
		"src": "data/json/texture_german_sniper_0.json"
	},
	{
		"name": "texture_heinz_prisoner_0",
		"type": "image",
		"src": "data/image/texture_heinz_prisoner_0.png"
	},
	{
		"name": "texture_heinz_prisoner_0",
		"type": "json",
		"src": "data/json/texture_heinz_prisoner_0.json"
	},
	{
		"name": "texture_image_0",
		"type": "image",
		"src": "data/image/texture_image_0.png"
	},
	{
		"name": "texture_image_0",
		"type": "json",
		"src": "data/json/texture_image_0.json"
	}
];
