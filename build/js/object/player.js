(function(){
// Put user code here //
 
//  End of user code  //

game.object.player = me.Sprite.extend({
	init: function(x, y, settings = {}){
		settings.texture = "army";
		settings.image = game.textureMap.get(settings.texture);
		var tempSprite = game.util.__populateAtlasIndices([
			"armi-1","armi-10","armi-11",
			"armi-12","armi-2","armi-3",
			"armi-4","armi-5","armi-6",
			"armi-7","armi-8","armi-9"
		], settings);
		settings.framewidth = settings.framewidth || 31;
		settings.frameheight = settings.frameheight || 36;
		settings.anchorPoint = {
			x: 0,
			y: 0
		};

        // Put user code here //
        
        //  End of user code  //

		this._super(me.Sprite, 'init', [x, y, settings]);

		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = true;
		this.updateWhenPaused = false;
		this.isPersistent = false;

		this.addAnimation('idle-bot', [{ name: "armi-2", delay: 100 }]);
		this.addAnimation('idle-up', [{ name: "armi-11", delay: 100 }]);
		this.addAnimation('idle-L', [{ name: "armi-5", delay: 100 }]);
		this.addAnimation('idle-R', [{ name: "armi-8", delay: 100 }]);
		this.addAnimation('walk-bot', [{ name: "armi-1", delay: 100 },{ name: "armi-2", delay: 100 },{ name: "armi-3", delay: 100 }]);
		this.addAnimation('walk-top', [{ name: "armi-10", delay: 100 },{ name: "armi-11", delay: 100 },{ name: "armi-12", delay: 100 }]);
		this.addAnimation('walk-L', [{ name: "armi-4", delay: 100 },{ name: "armi-5", delay: 100 },{ name: "armi-6", delay: 100 }]);
		this.addAnimation('walk-R', [{ name: "armi-7", delay: 100 },{ name: "armi-8", delay: 100 },{ name: "armi-9", delay: 100 }]);
		this.setCurrentAnimation('idle-bot');
		this.isKinematic = false;

		this.body = new me.Body(this);
		this.body.addShape( me.pool.pull("me.Rect", 0, 0, 31, 36) );
		this.body.collisionType = game.collisionTypes.PLAYER_OBJECT;
		this.body.setCollisionMask(game.collisionTypes.ALL_OBJECT);
		this.body.gravity.y = 0;
		this.var = {};

        // Put user code here //
        me.input.bindKey(me.input.KEY.LEFT,  "left");
        me.input.bindKey(me.input.KEY.RIGHT, "right");
        me.input.bindKey(me.input.KEY.UP, "up");
        me.input.bindKey(me.input.KEY.DOWN, "down");
        me.input.bindKey(me.input.KEY.SPACE, "space", true);
        
        this.state = {
            left: false,
            right : false,
            up : false,
            down : false
        }
        
        this.body.force.set(0, 0);
        this.body.friction.set(0.5, 0);
        this.body.setMaxVelocity(2, 2);
        this.body.falling = false;
        this.body.gravity.y = 0;
        
        this.stateChanged = false;
        
        this.nameText = me.pool.pull("me.Text", this.pos.x, this.pos.y - 50, {
                font: "Arial", 
                size: "40px", 
                fillStyle: "#FF0000",
                lineHeight: 1.3,
                textBaseline: "middle",
                textAlign: "left",
                anchorPoint: {x: 0.5, y: 0.5}
            });
        me.game.world.addChild(this.nameText, 102);
        this.nameText.setText(settings.id);
        
          
        this.hptext = me.pool.pull("me.Text", this.pos.x, this.pos.y - 25, {
                font: "Arial", 
                size: "30px", 
                fillStyle: "#FF0000",
                lineHeight: 1.3,
                textBaseline: "middle",
                textAlign: "left",
                anchorPoint: {x: 0.5, y: 0.5}
            });
        me.game.world.addChild(this.hptext, 102);
        this.hptext.setText(settings.hp);
        
        //console.log(this.hp)
        //this.posisi = "";
        this.totalhp = settings.hp;
        this.onhit = false;
        //  End of user code  //
	},

	update: function(dt){
		var drawNextFrame = this._super(me.Sprite, 'update', [dt]);

		this.body.update();
		me.collision.check(this);
		drawNextFrame = drawNextFrame || this.body.vel.x !== 0 || this.body.vel.y !== 0;
        // Put user code here //
        this.stateChanged = false;
        
        this.nameText.pos.x =  this.pos.x;
        this.nameText.pos.y = this.pos.y-50;
        
        this.hptext.pos.x =  this.pos.x;
        this.hptext.pos.y = this.pos.y-20;
        
        if (me.input.isKeyPressed("left")){
            //this.flipX(true);
            this.body.force.x = -this.body.maxVel.x;
            if (!this.isCurrentAnimation("walk-L")) {
                this.setCurrentAnimation("walk-L");
            }
            this.stateChanged = true;
            this.state.left = true;
            this.state.right = false;
            this.state.up = false;
            this.state.down = false;
            //this.posisi = "kiri";
            
        } else if (me.input.isKeyPressed("right")){
            //this.flipX(false);
            this.body.force.x = this.body.maxVel.x;
            if (!this.isCurrentAnimation("walk-R")) {
                this.setCurrentAnimation("walk-R");
            } 
            this.stateChanged = true;
            this.state.left = false;
            this.state.right = true;
            this.state.up = false;
            this.state.down = false;
            //this.posisi = "kanan";
           
        } else {
            this.body.force.x = 0;
            if(this.state.left == true){
                this.setCurrentAnimation("idle-L");
            }if(this.state.right == true){
                this.setCurrentAnimation("idle-R");
            }
        }
        
        if(me.input.isKeyPressed("up")){
            this.body.force.y = -this.body.maxVel.y;
             this.body.maxVel.y = 2;
            if (!this.isCurrentAnimation("walk-top")) {
                this.setCurrentAnimation("walk-top");
            } 
            this.stateChanged = true;
            this.state.left = false;
            this.state.right = false;
            this.state.up = true;
            this.state.down = false;
            //this.posisi = "atas";
            
        }else if(me.input.isKeyPressed("down")){
            this.body.force.y = this.body.maxVel.y;
             this.body.maxVel.y = 2;
            if (!this.isCurrentAnimation("walk-bot")) {
                this.setCurrentAnimation("walk-bot");
            } 
            this.stateChanged = true;
            this.state.left = false;
            this.state.right = false;
            this.state.up = false;
            this.state.down = true;
            //this.posisi = "bawah";
            
        } else {
            this.body.force.y = 0;
            this.body.maxVel.y = 0;
            if(this.state.up == true){
                this.setCurrentAnimation("idle-up");
            }if(this.state.down == true){
                this.setCurrentAnimation("idle-bot");
            }
        }
        
  
        if(me.input.isKeyPressed("space")){
            
            game.var.socket.emit('nembak', {id: game.data.playerId, posx: this.pos.x, posy: this.pos.y});
            var peluru = me.pool.pull("peluru", this.pos.x, this.pos.y,{
                status:this
            });
            game.data.pelurulist[game.data.playerId] = peluru;
            me.game.world.addChild(peluru, 101);
        }
        
        if (this.stateChanged) {
            game.var.socket.emit('updatePlayerState', {
                x: this.pos.x,
                y: this.pos.y
            }, this.state);
            return true;
        } else {
            return false;
        }
        
        //  End of user code  //
		return drawNextFrame;
	},

	onCollision : function(response, other) {
		var isSolid = true;
        // Put user code here //
        isSolid = false;
        if(!this.onhit) {  
            if (other.body.collisionType === game.collisionTypes.PROJECTILE_OBJECT) {
                this.onhit = true;
                console.log("player kena peluru");
                this.totalhp -=1;
                game.var.socket.emit('hpngurang', {id: game.data.playerId, hp: this.totalhp});
                this.hptext.setText( this.totalhp);
                this.delay = me.timer.setTimeout(()=>{
                  this.onhit = false;
                },500);
            }
           
        }
       
        //  End of user code  //
		return isSolid;
	},

	draw : function(renderer, rect) {
		this._super(me.Sprite, 'draw', [renderer, rect]);
        // Put user code here //
        
        //  End of user code  //
	},

	onActivateEvent : function() {
        // Put user code here //
        
        //  End of user code  //
	},

	onDeactivateEvent : function() {

        // Put user code here //
        me.timer.clearTimeout(this.delay);
        //  End of user code  //
	},

    // Put user code here //
    
    //  End of user code  //
});

// Put user code here //
 
//  End of user code  //
})();