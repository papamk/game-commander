(function(){
// Put user code here //
 
//  End of user code  //

game.object.enemy = me.Sprite.extend({
	init: function(x, y, settings = {}){
		settings.texture = "army";
		settings.image = game.textureMap.get(settings.texture);
		var tempSprite = game.util.__populateAtlasIndices([
			"armi-1","armi-2","armi-3",
			"armi-4","armi-5","armi-6",
			"armi-7","armi-8","armi-9",
			"armi-10","armi-11","armi-12"
		], settings);
		settings.framewidth = settings.framewidth || 100;
		settings.frameheight = settings.frameheight || 100;
		settings.anchorPoint = {
			x: 0,
			y: 0
		};

        // Put user code here //
        
        //  End of user code  //

		this._super(me.Sprite, 'init', [x, y, settings]);

		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = false;
		this.updateWhenPaused = false;
		this.isPersistent = false;

		this.addAnimation('idle-down', [{ name: "armi-2", delay: 100 }]);
		this.addAnimation('idle-right', [{ name: "armi-8", delay: 100 }]);
		this.addAnimation('idle-left', [{ name: "armi-5", delay: 100 }]);
		this.addAnimation('walk-down', [{ name: "armi-1", delay: 100 },{ name: "armi-2", delay: 100 },{ name: "armi-3", delay: 100 }]);
		this.addAnimation('walk-left', [{ name: "armi-4", delay: 100 },{ name: "armi-5", delay: 100 },{ name: "armi-6", delay: 100 }]);
		this.addAnimation('walk-right', [{ name: "armi-7", delay: 100 },{ name: "armi-8", delay: 100 },{ name: "armi-9", delay: 100 }]);
		this.addAnimation('walk-up', [{ name: "armi-10", delay: 100 },{ name: "armi-11", delay: 100 },{ name: "armi-12", delay: 100 }]);
		this.addAnimation('idle-up', [{ name: "armi-11", delay: 100 }]);
		this.setCurrentAnimation('idle-down');
		this.isKinematic = false;

		this.body = new me.Body(this);
		this.body.addShape( me.pool.pull("me.Rect", 0, 0, 100, 100) );
		this.body.addShape( me.pool.pull("me.Rect", 0, 0, 100, 100) );
		this.body.setCollisionMask(game.collisionTypes.ALL_OBJECT);
		this.body.gravity.y = 0;
		this.var = {};

        // Put user code here //
        this.state = {};
        
        this.nameText = me.pool.pull("me.Text", this.pos.x, this.pos.y - 130, {
            font: "Arial", 
            size: "50px", 
            fillStyle: "#FF0000",
            lineHeight: 1.3,
            textBaseline: "middle",
            textAlign: "left",
            anchorPoint: {x: 0.5, y: 0.5}
        });
        me.game.world.addChild(this.nameText, 102);
        this.nameText.setText(settings.id);
        
        this.hptext = me.pool.pull("me.Text", this.pos.x, this.pos.y - 25, {
                font: "Arial", 
                size: "30px", 
                fillStyle: "#FF0000",
                lineHeight: 1.3,
                textBaseline: "middle",
                textAlign: "left",
                anchorPoint: {x: 0.5, y: 0.5}
            });
        me.game.world.addChild(this.hptext, 102);
        this.hptext.setText(settings.hp);
        this.hp = settings.hp;
        //  End of user code  //
	},

	update: function(dt){
		var drawNextFrame = this._super(me.Sprite, 'update', [dt]);

		this.body.update();
		me.collision.check(this);
		drawNextFrame = drawNextFrame || this.body.vel.x !== 0 || this.body.vel.y !== 0;
        // Put user code here //
            this.nameText.pos.x =  this.pos.x;
            this.nameText.pos.y = this.pos.y-50;
            
            this.hptext.pos.x =  this.pos.x;
            this.hptext.pos.y = this.pos.y-20;
            
            if(this.state.left){
                if (!this.isCurrentAnimation("walk-left")) {
                    this.setCurrentAnimation("walk-left");
                }
            }
            else if(this.state.right){
                if (!this.isCurrentAnimation("walk-right")) {
                    this.setCurrentAnimation("walk-right");
                }
            }
            else if(this.state.up){
                if (!this.isCurrentAnimation("walk-up")) {
                    this.setCurrentAnimation("walk-up");
                }
            }
            else if(this.state.down){
                if (!this.isCurrentAnimation("walk-down")) {
                    this.setCurrentAnimation("walk-down");
                }
            }else{
                if(this.state.up == true){
                    this.setCurrentAnimation("idle-up");
                }if(this.state.down == true){
                    this.setCurrentAnimation("idle-down");
                }
                if(this.state.left == true){
                    this.setCurrentAnimation("idle-left");
                }if(this.state.right == true){
                    this.setCurrentAnimation("idle-right");
                }   
            }
            
        this.state = {};
        this.hptext.setText(this.hp);
        //  End of user code  //
		return drawNextFrame;
	},

	onCollision : function(response, other) {
		var isSolid = true;
        // Put user code here //
        isSolid = false;
        // if (other.body.collisionType === game.collisionTypes.PROJECTILE_OBJECT) {
        //     console.log("peluru kena enemy ");
        // }
        //  End of user code  //
		return isSolid;
	},

	draw : function(renderer, rect) {
		this._super(me.Sprite, 'draw', [renderer, rect]);
        // Put user code here //
        
        //  End of user code  //
	},

	onActivateEvent : function() {
        // Put user code here //
        
        //  End of user code  //
	},

	onDeactivateEvent : function() {

        // Put user code here //
    me.game.world.removeChild(this.nameText);
    me.game.world.removeChild(this.hptext);
        //  End of user code  //
	},

    // Put user code here //
    
    //  End of user code  //
});

// Put user code here //
 
//  End of user code  //
})();