(function(){
// Put user code here //
 
//  End of user code  //

game.object.peluru = me.Sprite.extend({
	init: function(x, y, settings = {}){
		settings.texture = "image";
		settings.image = game.textureMap.get(settings.texture);
		var tempSprite = game.util.__populateAtlasIndices([
			"ammo"
		], settings);
		settings.framewidth = settings.framewidth || 31;
		settings.frameheight = settings.frameheight || 36;
		settings.anchorPoint = {
			x: 0,
			y: 0
		};

        // Put user code here //
        
        //  End of user code  //

		this._super(me.Sprite, 'init', [x, y, settings]);

		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = true;
		this.updateWhenPaused = false;
		this.isPersistent = false;
		this.isKinematic = false;

		this.body = new me.Body(this);
		this.body.addShape( me.pool.pull("me.Rect", 0, 0, 31, 36) );
		this.body.collisionType = game.collisionTypes.ACTION_OBJECT;
		this.body.setCollisionMask(game.collisionTypes.ALL_OBJECT);
		this.body.gravity.y = 0;
		this.var = {};

        // Put user code here //
            
        let vars = ["status"];
        verify(settings, vars);
        game.util.spread(this, settings, vars);
        
        this.stateChanged = false;
        
        this.body.force.set(0, 0);
        this.body.friction.set(0.5, 0);
        this.body.setMaxVelocity(10, 10);
        this.body.falling = false;
        this.body.gravity.y = 0;
        
        if(this.status.state.left == true){
            this.body.force.set(10,0);
            this.body.force.x = -this.body.maxVel.x;
            this.stateChanged = true;
            return false;
        }else if(this.status.state.right == true){
            this.body.force.set(10,0);
            this.body.force.x = this.body.maxVel.x;
            this.stateChanged = true;
             return false;
        }else if(this.status.state.up == true){
            this.body.force.set(0,10);
            this.body.force.y = -this.body.maxVel.y;
            this.stateChanged = true;
             return false;
        }else if(this.status.state.down == true){
            this.body.force.set(0,10);
            this.body.force.y = this.body.maxVel.y;
            this.stateChanged = true;
             return false;
        }
    
        //  End of user code  //
	},

	update: function(dt){
		var drawNextFrame = this._super(me.Sprite, 'update', [dt]);

		this.body.update();
		me.collision.check(this);
		drawNextFrame = drawNextFrame || this.body.vel.x !== 0 || this.body.vel.y !== 0;
        // Put user code here //
        
        let limitHeight = 0,
		    limitWidthLeft  = 0,
		    limitWidthRight = 1920,
		    limitbawah = 1080;
		    
        if (this.pos.y < limitHeight) { //limit Atas
		    me.game.world.removeChild(this);
		    return false;
        }
        if (this.pos.x < limitWidthLeft) { //limit Kiri
		    me.game.world.removeChild(this);
		    return false;
        }
        if (this.pos.x > limitWidthRight) { //limit Kanan
		    me.game.world.removeChild(this);
		    return false;
        }
        if (this.pos.y > limitbawah) { //limit Kanan
		    me.game.world.removeChild(this);
		    return false;
        }
          
        game.var.socket.emit('updateposisipeluru', {
            x: this.pos.x,
            y: this.pos.y
        }, this.state);
     
        //  End of user code  //
		return drawNextFrame;
	},

	onCollision : function(response, other) {
		var isSolid = true;
        // Put user code here //
        isSolid = false;
        if (other.body.collisionType === game.collisionTypes.ENEMY_OBJECT) {
            //console.log("peluru kena object ");
            me.game.world.removeChild(this);
        }
        //  End of user code  //
		return isSolid;
	},

	draw : function(renderer, rect) {
		this._super(me.Sprite, 'draw', [renderer, rect]);
        // Put user code here //
 
        //  End of user code  //
	},

	onActivateEvent : function() {
        // Put user code here //
        
        //  End of user code  //
	},

	onDeactivateEvent : function() {

        // Put user code here //
        delete game.data.pelurulist[game.data.playerId];
        //  End of user code  //
	},

    // Put user code here //
    
    //  End of user code  //
});

// Put user code here //
 
//  End of user code  //
})();