(function(){
// Put user code here //
 
//  End of user code  //

game.object.jerman_sniper = me.Sprite.extend({
	init: function(x, y, settings = {}){
		settings.texture = "german_sniper";
		settings.image = game.textureMap.get(settings.texture);
		var tempSprite = game.util.__populateAtlasIndices([
			"German_sniper_attack_back0","German_sniper_attack_back1","German_sniper_attack_back2",
			"German_sniper_attack_back3","German_sniper_attack_back4","German_sniper_attack_back6",
			"German_sniper_attack_back5","German_sniper_attack_back7","German_sniper_attack_front_0",
			"German_sniper_attack_front_1","German_sniper_attack_front_2","German_sniper_attack_front_3",
			"German_sniper_attack_front_4","German_sniper_attack_front_5","German_sniper_attack_front_6",
			"German_sniper_attack_front_7","German_sniper_dead0","German_sniper_dead1",
			"German_sniper_dead2","German_sniper_side_attack0","German_sniper_side_attack1",
			"German_sniper_side_attack2","German_sniper_side_attack3","German_sniper_side_attack4",
			"German_sniper_side_attack5","German_sniper_side_attack6","German_sniper_side_attack7",
			"German_sniper_side_walk0","German_sniper_side_walk1","german_sniper_walk_front0",
			"german_sniper_walk_front1","German_walk_back0","German_walk_back1"
		], settings);
		settings.framewidth = settings.framewidth || 100;
		settings.frameheight = settings.frameheight || 100;
		settings.anchorPoint = {
			x: 0,
			y: 0
		};

        // Put user code here //
        
        //  End of user code  //

		this._super(me.Sprite, 'init', [x, y, settings]);

		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = true;
		this.updateWhenPaused = false;
		this.isPersistent = false;

		this.addAnimation('attack_top', [{ name: "German_sniper_attack_back0", delay: 100 },{ name: "German_sniper_attack_back1", delay: 100 },{ name: "German_sniper_attack_back2", delay: 100 },{ name: "German_sniper_attack_back3", delay: 100 },{ name: "German_sniper_attack_back4", delay: 100 },{ name: "German_sniper_attack_back6", delay: 100 },{ name: "German_sniper_attack_back5", delay: 100 },{ name: "German_sniper_attack_back7", delay: 100 }]);
		this.addAnimation('attack_bot', [{ name: "German_sniper_attack_front_0", delay: 100 },{ name: "German_sniper_attack_front_1", delay: 100 },{ name: "German_sniper_attack_front_2", delay: 100 },{ name: "German_sniper_attack_front_3", delay: 100 },{ name: "German_sniper_attack_front_4", delay: 100 },{ name: "German_sniper_attack_front_5", delay: 100 },{ name: "German_sniper_attack_front_6", delay: 100 },{ name: "German_sniper_attack_front_7", delay: 100 }]);
		this.addAnimation('dead', [{ name: "German_sniper_dead0", delay: 200 },{ name: "German_sniper_dead1", delay: 200 },{ name: "German_sniper_dead2", delay: 200 }]);
		this.addAnimation('attack_R', [{ name: "German_sniper_side_attack0", delay: 100 },{ name: "German_sniper_side_attack1", delay: 100 },{ name: "German_sniper_side_attack2", delay: 100 },{ name: "German_sniper_side_attack3", delay: 100 },{ name: "German_sniper_side_attack4", delay: 100 },{ name: "German_sniper_side_attack5", delay: 100 },{ name: "German_sniper_side_attack6", delay: 100 },{ name: "German_sniper_side_attack7", delay: 100 }]);
		this.addAnimation('walk_R', [{ name: "German_sniper_side_walk0", delay: 100 },{ name: "German_sniper_side_walk1", delay: 100 }]);
		this.addAnimation('walk_bot', [{ name: "german_sniper_walk_front0", delay: 100 },{ name: "german_sniper_walk_front1", delay: 100 }]);
		this.addAnimation('walk_top', [{ name: "German_walk_back0", delay: 100 },{ name: "German_walk_back1", delay: 100 }]);
		this.addAnimation('idle_bot', [{ name: "german_sniper_walk_front0", delay: 100 }]);
		this.addAnimation('idle_top', [{ name: "German_walk_back0", delay: 100 }]);
		this.addAnimation('idle_R', [{ name: "German_sniper_side_walk0", delay: 100 }]);
		this.setCurrentAnimation('attack_top');
		this.isKinematic = false;

		this.body = new me.Body(this);
		this.body.addShape(me.pool.pull("me.Rect", 0, 0, this.width, this.height) );
		this.body.setCollisionMask(game.collisionTypes.PLAYER_OBJECT);
		this.body.gravity.y = 0;
		this.var = {};

        // Put user code here //
        this.component = {};
        this.setCurrentAnimation('idle_R');
        this.radius = 300;
        this.isPlayerSide = false;
        this.body.collisionType = me.collision.types.ENEMY_OBJECT;
        
        this.component.colliderAttack = me.pool.pull("game.component.collider", this, {
            radius: this.radius,
            colliderType: game.collisionTypes.RANGE_ATTACK,
            colliderMask: this.isPlayerSide ? 
            me.collision.types.ENEMY_OBJECT : me.collision.types.PLAYER_OBJECT
        });
       
        
        this.behaviourJSON = me.plugins.Behaviour.convertJSONString(unitRangeBehaviourAI);
        //  End of user code  //
	},

	update: function(dt){
		var drawNextFrame = this._super(me.Sprite, 'update', [dt]);

		this.body.update();
		me.collision.check(this);
		drawNextFrame = drawNextFrame || this.body.vel.x !== 0 || this.body.vel.y !== 0;
        // Put user code here //
        this.component.colliderAttack.update(dt);
        me.plugins.Behaviour.executeJsonBehaviour(this.behaviourJSON, this);
        //  End of user code  //
		return drawNextFrame;
	},

	onCollision : function(response, other) {
		var isSolid = true;
        // Put user code here //
        isSolid = false;
        //  End of user code  //
		return isSolid;
	},

	draw : function(renderer, rect) {
		this._super(me.Sprite, 'draw', [renderer, rect]);
        // Put user code here //
        
        //  End of user code  //
	},

	onActivateEvent : function() {
        // Put user code here //
        
        //  End of user code  //
	},

	onDeactivateEvent : function() {

        // Put user code here //
        
        //  End of user code  //
	},

    // Put user code here //
    isTargetInRange: function(){
        var listInRange = [];
        this.component.colliderAttack.onCollide = (response, other) => {
            if(this.checkTarget(other)){
                listInRange.push(other);
            }
        };
        this.component.colliderAttack.checkCollision();
        console.log("[BEHAVIOUR] IsTargetInRange");
        return listInRange.includes(this.target);
    },
    
    attackTarget: function(){
        this.component.move.stopMovement();
        this.changeAnimation("attack");
        
        if(this.component.attack.checkAttackFrame()){
            console.log("[BEHAVIOUR] AttackTarget");
            this._attackTarget();
        }
        return true;
    },
    
    _attackTarget: function(){
        this.component.attack.attackTarget(1 / this.attackPerCycle);
    },
    
    findTarget: function(){
        var res = false;
        this.component.colliderAttack.onCollide = (response, other) => {
            if(this.checkTarget(other)){
                res = true;
                this.target = other;
                console.log("targeted respon");
            }
        };
        this.component.colliderAttack.checkCollision();
         console.log("[BEHAVIOUR] FindTarget", res);
        return res;
    },
    
    checkTarget: function(target){
        return target.side !== this.side;
    },
    
    //coba fungsi
    
    haveTarget: function(){ return 
    //console.log("havetarget");
    },
    ifDead: function(){ return 
    //console.log("dead") 
    },
    checkEffect: function(){ return
    //console.log("cek efek")
    },   
    
    destroyed: function(){
  
        //console.log("DESTROYED");
        return true;
    },
    
    moveForward: function(){
     
        return true;
    },
    //  End of user code  //
});

// Put user code here //
 
//  End of user code  //
})();