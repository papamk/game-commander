(function(){
// Put user code here //
 
//  End of user code  //

game.object.pelurulain = me.Sprite.extend({
	init: function(x, y, settings = {}){
		settings.texture = "image";
		settings.image = game.textureMap.get(settings.texture);
		var tempSprite = game.util.__populateAtlasIndices([
			"ammo"
		], settings);
		settings.framewidth = settings.framewidth || 31;
		settings.frameheight = settings.frameheight || 36;
		settings.anchorPoint = {
			x: 0,
			y: 0
		};

        // Put user code here //
        
        //  End of user code  //

		this._super(me.Sprite, 'init', [x, y, settings]);

		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = false;
		this.updateWhenPaused = false;
		this.isPersistent = false;
		this.isKinematic = false;

		this.body = new me.Body(this);
		this.body.addShape( me.pool.pull("me.Rect", 0, 0, 31, 36) );
		this.body.collisionType = game.collisionTypes.PROJECTILE_OBJECT;
		this.body.setCollisionMask(game.collisionTypes.ALL_OBJECT);
		this.body.gravity.y = 0;
		this.var = {};

        // Put user code here //
        this.state = {};
        //  End of user code  //
	},

	update: function(dt){
		var drawNextFrame = this._super(me.Sprite, 'update', [dt]);

		this.body.update();
		me.collision.check(this);
		drawNextFrame = drawNextFrame || this.body.vel.x !== 0 || this.body.vel.y !== 0;
        // Put user code here //
        this.state = {};
        
        let limitHeight = 0,
		    limitWidthLeft  = 0,
		    limitWidthRight = 1920,
		    limitbawah = 1080;
		    
        if (this.pos.y < limitHeight) { //limit Atas
		    me.game.world.removeChild(this);
		    return false;
        }
        if (this.pos.x < limitWidthLeft) { //limit Kiri
		    me.game.world.removeChild(this);
		    return false;
        }
        if (this.pos.x > limitWidthRight) { //limit Kanan
		    me.game.world.removeChild(this);
		    return false;
        }
        if (this.pos.y > limitbawah) { //limit Kanan
		    me.game.world.removeChild(this);
		    return false;
        }
        //  End of user code  //
		return drawNextFrame;
	},

	onCollision : function(response, other) {
		var isSolid = true;
        // Put user code here //
        isSolid = false;
        if (other.body.collisionType === game.collisionTypes.PLAYER_OBJECT) {
            //delete game.data.pelurulist[game.data.playerId];
            me.game.world.removeChild(this);
        }
        //  End of user code  //
		return isSolid;
	},

	draw : function(renderer, rect) {
		this._super(me.Sprite, 'draw', [renderer, rect]);
        // Put user code here //
        
        //  End of user code  //
	},

	onActivateEvent : function() {
        // Put user code here //
        
        //  End of user code  //
	},

	onDeactivateEvent : function() {

        // Put user code here //
        
        //  End of user code  //
	},

    // Put user code here //
    
    //  End of user code  //
});

// Put user code here //
 
//  End of user code  //
})();