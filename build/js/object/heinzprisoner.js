(function(){
// Put user code here //
 
//  End of user code  //

game.object.heinzprisoner = me.Sprite.extend({
	init: function(x, y, settings = {}){
		settings.texture = "heinz_prisoner";
		settings.image = game.textureMap.get(settings.texture);
		var tempSprite = game.util.__populateAtlasIndices([
			"heinz_back_walk0","heinz_back_walk1","Heinz_dead0",
			"Heinz_dead1","Heinz_dead2","Heinz_side_walk0",
			"Heinz_side_walk1","heinz_walk0","heinz_walk1"
		], settings);
		settings.framewidth = settings.framewidth || 100;
		settings.frameheight = settings.frameheight || 100;
		settings.anchorPoint = {
			x: 0,
			y: 0
		};

        // Put user code here //
        
        //  End of user code  //

		this._super(me.Sprite, 'init', [x, y, settings]);

		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = true;
		this.updateWhenPaused = false;
		this.isPersistent = false;

		this.addAnimation('walk_R', [{ name: "Heinz_side_walk1", delay: 100 },{ name: "Heinz_side_walk0", delay: 100 }]);
		this.addAnimation('walk_bot', [{ name: "heinz_walk0", delay: 100 },{ name: "heinz_walk1", delay: 100 }]);
		this.addAnimation('walk_top', [{ name: "heinz_back_walk0", delay: 100 },{ name: "heinz_back_walk1", delay: 100 }]);
		this.addAnimation('idle_bot', [{ name: "heinz_walk0", delay: 100 }]);
		this.addAnimation('idle_top', [{ name: "heinz_back_walk0", delay: 100 }]);
		this.addAnimation('idle_R', [{ name: "Heinz_side_walk1", delay: 100 }]);
		this.setCurrentAnimation('walk_R');
		this.isKinematic = false;

		this.body = new me.Body(this);
		this.body.addShape( me.pool.pull("me.Rect", 0, 0, 100, 100) );
		this.body.collisionType = game.collisionTypes.PLAYER_OBJECT;
		this.body.setCollisionMask(game.collisionTypes.ALL_OBJECT);
		this.body.gravity.y = 0;
		this.var = {};

        // Put user code here //
        me.input.bindKey(me.input.KEY.LEFT,  "left");
        me.input.bindKey(me.input.KEY.RIGHT, "right");
        me.input.bindKey(me.input.KEY.UP, "up");
        me.input.bindKey(me.input.KEY.DOWN, "down");
        
        
        this.speed = 2;
        
        this.state = {
            left: false,
            right : false,
            up : false,
            down : false,
            walk : false
        }
        
        this.body.force.set(0, 0);
        this.body.friction.set(0.5, 0);
        this.body.setMaxVelocity( this.speed, this.speed);
        this.body.falling = false;
        this.body.gravity.y = 0;
        
        this.stateChanged = false;
        
        this.state.left = true;
        
        this.shadow = me.pool.pull("shadow", this.pos.x-3480, this.pos.y-1620);
        me.game.world.addChild(this.shadow, this.pos.z+100);
        
        this.kuning = me.pool.pull("kuning", this.pos.x, this.pos.y);
        me.game.world.addChild(this.kuning, this.pos.z+99)
        
        // this.body.collisionTypes = me.collision.types.PLAYER_OBJECT;
        
        //  End of user code  //
	},

	update: function(dt){
		var drawNextFrame = this._super(me.Sprite, 'update', [dt]);

		this.body.update();
		me.collision.check(this);
		drawNextFrame = drawNextFrame || this.body.vel.x !== 0 || this.body.vel.y !== 0;
        // Put user code here //
        if(me.input.isKeyPressed("left")){
            this.flipX(true);
            // this.body.force.x = -this.body.maxVel.x;
            this.body.vel.x = -this.speed;
            if (!this.isCurrentAnimation("walk_R")) {
                this.setCurrentAnimation("walk_R");
            }
            this.stateChanged = true;
            this.state.left = true;
            this.state.right = false;
            this.state.up = false;
            this.state.down = false;
            this.state.walk = true;

        }else if (me.input.isKeyPressed("right")){
            this.flipX(false);
            //this.body.force.x = this.body.maxVel.x;
            this.body.vel.x = this.speed;
            if (!this.isCurrentAnimation("walk_R")) {
                this.setCurrentAnimation("walk_R");
            } 
            this.stateChanged = true;
            this.state.left = false;
            this.state.right = true;
            this.state.up = false;
            this.state.down = false;
            this.state.walk = true;
        }else {
            this.body.force.x = 0;
            if(this.state.left == true ){
                this.flipX(true);
                this.setCurrentAnimation("idle_R");
            }if(this.state.right == true ){
                this.flipX(false);
                this.setCurrentAnimation("idle_R");
            }
        }
        
        if(me.input.isKeyPressed("up")){
            this.body.vel.y = -this.speed;
            // this.body.force.y = -this.body.maxVel.y;
            this.body.maxVel.y = 2;
            if (!this.isCurrentAnimation("walk_top")) {
                this.setCurrentAnimation("walk_top");
            } 
            this.stateChanged = true;
            this.state.left = false;
            this.state.right = false;
            this.state.up = true;
            this.state.down = false;
            
        }else if(me.input.isKeyPressed("down")){
            // this.body.force.y = this.body.maxVel.y;
           this.body.maxVel.y = 2;
           this.body.vel.y = this.speed;
            if (!this.isCurrentAnimation("walk_bot")) {
                this.setCurrentAnimation("walk_bot");
            } 
            this.stateChanged = true;
            this.state.left = false;
            this.state.right = false;
            this.state.up = false;
            this.state.down = true;

         } else {
            this.body.force.y = 0;
            this.body.maxVel.y = 0;
            //console.log("stop");
            if(this.state.up == true){
                this.setCurrentAnimation("idle_top");
            }if(this.state.down == true){
               this.setCurrentAnimation("idle_bot");
            }
        }
        
        
        this.shadow.pos.x = this.pos.x-3480;
        this.shadow.pos.y = this.pos.y-1620;
        
        this.kuning.pos.x = this.pos.x;
        this.kuning.pos.y = this.pos.y;
        
        
        //  End of user code  //
		return drawNextFrame;
	},

	onCollision : function(response, other) {
		var isSolid = true;
        // Put user code here //
        	var isSolid = false;
        //  End of user code  //
		return isSolid;
	},

	draw : function(renderer, rect) {
		this._super(me.Sprite, 'draw', [renderer, rect]);
        // Put user code here //
        
        //  End of user code  //
	},

	onActivateEvent : function() {
        // Put user code here //
        
        //  End of user code  //
	},

	onDeactivateEvent : function() {

        // Put user code here //
        
        //  End of user code  //
	},

    // Put user code here //
    
    //  End of user code  //
});

// Put user code here //
 
//  End of user code  //
})();