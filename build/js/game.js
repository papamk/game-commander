var game = {
	var : {},

	object : {},

	level : {},

	state : {},

	util : {},

	collisionTypes : me.collision.types,

	onload : function () {
		// Initialize the video.
		if (!me.video.init(1920, 1080, {wrapper : "screen", scale : "auto", scaleMethod: "fit"})) {
			alert("Your browser does not support HTML5 canvas.");
			return;
		}
		me.audio.init("mp3,ogg");

		// set and load all resources.
		// (this will also automatically switch to the loading screen)
		me.loader.preload(game.resources, this.loaded.bind(this));
	},

	// Run on game resources loaded.
	loaded : function () {
		game.state.level1 = "level1";
		me.state.set("level1", new game.level.level1());
		game.state.prisoner1 = "prisoner1";
		me.state.set("prisoner1", new game.level.prisoner1());

		me.pool.register('heinzprisoner', game.object.heinzprisoner);
		me.pool.register('jerman_sniper', game.object.jerman_sniper);
		me.pool.register('kuning', game.object.kuning);
		me.pool.register('shadow', game.object.shadow);
		me.pool.register('box_wall', game.object.box_wall);
		me.pool.register('box_wall_b', game.object.box_wall_b);
		me.pool.register('jeep_h', game.object.jeep_h);
		me.pool.register('jeep_v', game.object.jeep_v);
		me.pool.register('enemy', game.object.enemy);
		me.pool.register('peluru', game.object.peluru);
		me.pool.register('pelurulain', game.object.pelurulain);
		me.pool.register('player', game.object.player);

		game.textureMap = new Map();
		game.textureMap.set("army", new me.video.renderer.Texture([
			me.loader.getJSON("texture_army_0")
		], undefined, false));

		game.textureMap.set("bglv", new me.video.renderer.Texture([
			me.loader.getJSON("texture_bglv_0")
		], undefined, false));

		game.textureMap.set("env", new me.video.renderer.Texture([
			me.loader.getJSON("texture_env_0")
		], undefined, false));

		game.textureMap.set("german_sniper", new me.video.renderer.Texture([
			me.loader.getJSON("texture_german_sniper_0")
		], undefined, false));

		game.textureMap.set("heinz_prisoner", new me.video.renderer.Texture([
			me.loader.getJSON("texture_heinz_prisoner_0")
		], undefined, false));

		game.textureMap.set("image", new me.video.renderer.Texture([
			me.loader.getJSON("texture_image_0")
		], undefined, false));

		game.imageLocation = {
			"ammo": "image",
			"armi-1": "army",
			"armi-10": "army",
			"armi-11": "army",
			"armi-12": "army",
			"armi-2": "army",
			"armi-3": "army",
			"armi-4": "army",
			"armi-5": "army",
			"armi-6": "army",
			"armi-7": "army",
			"armi-8": "army",
			"armi-9": "army",
			"bg": "image",
			"kuning": "bglv",
			"level_prison_lv1": "bglv",
			"level_prison_lv1_half": "bglv",
			"shadow": "bglv",
			"German_sniper_attack_back0": "german sniper",
			"German_sniper_attack_back1": "german sniper",
			"German_sniper_attack_back2": "german sniper",
			"German_sniper_attack_back3": "german sniper",
			"German_sniper_attack_back4": "german sniper",
			"German_sniper_attack_back5": "german sniper",
			"German_sniper_attack_back6": "german sniper",
			"German_sniper_attack_back7": "german sniper",
			"German_sniper_attack_front_0": "german sniper",
			"German_sniper_attack_front_1": "german sniper",
			"German_sniper_attack_front_2": "german sniper",
			"German_sniper_attack_front_3": "german sniper",
			"German_sniper_attack_front_4": "german sniper",
			"German_sniper_attack_front_5": "german sniper",
			"German_sniper_attack_front_6": "german sniper",
			"German_sniper_attack_front_7": "german sniper",
			"German_sniper_dead0": "german sniper",
			"German_sniper_dead1": "german sniper",
			"German_sniper_dead2": "german sniper",
			"German_sniper_side_attack0": "german sniper",
			"German_sniper_side_attack1": "german sniper",
			"German_sniper_side_attack2": "german sniper",
			"German_sniper_side_attack3": "german sniper",
			"German_sniper_side_attack4": "german sniper",
			"German_sniper_side_attack5": "german sniper",
			"German_sniper_side_attack6": "german sniper",
			"German_sniper_side_attack7": "german sniper",
			"German_sniper_side_walk0": "german sniper",
			"German_sniper_side_walk1": "german sniper",
			"german_sniper_walk_front0": "german sniper",
			"german_sniper_walk_front1": "german sniper",
			"German_walk_back0": "german sniper",
			"German_walk_back1": "german sniper",
			"box_wall": "env",
			"box_wall_b": "env",
			"jeep_h": "env",
			"jeep_v": "env",
			"heinz_back_walk0": "heinz prisoner",
			"heinz_back_walk1": "heinz prisoner",
			"Heinz_dead0": "heinz prisoner",
			"Heinz_dead1": "heinz prisoner",
			"Heinz_dead2": "heinz prisoner",
			"Heinz_side_walk0": "heinz prisoner",
			"Heinz_side_walk1": "heinz prisoner",
			"heinz_walk0": "heinz prisoner",
			"heinz_walk1": "heinz prisoner",
			"map": "image",
		};


		game.util.__populateAtlasIndices = function(animationKeys, settings){
			let tpAtlas = [], indices = {},
				width = 0, height = 0,
				texture = game.textureMap.get(settings.texture);
			for (let i = 0; i < animationKeys.length; i++) {
				let region = texture.getRegion(animationKeys[i]);
				if (region == null) {
					// throw an error
					throw new me.video.renderer.Texture.Error(
						"Texture - region for " + animationKeys[i] + " not found");
				}
				tpAtlas[i] = region;
				indices[animationKeys[i]] = i;
				width = Math.max(region.width, width);
				height = Math.max(region.height, height);
			}
			settings.framewidth = width;
			settings.frameheight = height;
			settings.atlas = tpAtlas;
			settings.atlasIndices = indices;
		}

		game.object.__spriteTP = me.Sprite.extend({
			init: function(x, y, settings = {}){
				settings.image = (settings.texture) ? game.textureMap.get(settings.texture) : settings.region;
				settings.anchorPoint = settings.anchorPoint || {
					x : 0.5,
					y: 0.5
				}
			this._super(me.Sprite, 'init', [x, y, settings]);

			this.alpha = 1;
			this.floating = false;
			this.alwaysUpdate = false;
			this.updateWhenPaused = false;
			this.isPersistent = false;

			this.imageName = settings.region;

			},
		});
        // Put user code here //
        
        //  End of user code  //
		me.pool.register('spriteTP', game.object.__spriteTP);
		me.state.change("prisoner1");
	}
};
